---
title: "Gitlab 13.11.2 feature update"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2021-05-03 22:40:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2021-05-04 00:04:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
- gitlab


# Don't change the value below
section: issue
---
We have updated GitLab to 13.11.2 feature release.
