---
title: "codema.in sheduled maintenance"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2020-07-12 02:30:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2020-07-12 03:25:00
# You can use: down, disrupted, notice
severity: down
# affected sections (array). Uncomment the affected one's
affected:
 - loomio

# Don't change the value below
section: issue
---
