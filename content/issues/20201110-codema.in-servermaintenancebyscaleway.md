---
title: "codema.in scaleway migration maintenance"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2020-11-10 16:30:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2020-11-10 17:30:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
 - loomio

# Don't change the value below
section: issue
---
scaleway completed the migration of codema vm to new physical hypervisor
