---
title: "Gitlab 13.9.6 security update"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2021-04-16 22:55:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2021-04-16 23:53:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
- gitlab


# Don't change the value below
section: issue
---
We have updated GitLab to 13.9.6 security update.
