---
title: "Intermittent service outage"
date: 2021-08-19 14:30:00
resolved: true
resolvedWhen: 2021-08-19 17:00:00
severity: notice
affected:
- gitlab
section: issue
---

There will be intermittent outages on gitlab while we are configuring Rack::Attack to prevent brute force attacks.
