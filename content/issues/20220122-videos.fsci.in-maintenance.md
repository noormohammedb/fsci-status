---
title: "Peertube Maintenance"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2022-01-14 14:00:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2022-01-22 01:40:00
# You can use: down, disrupted, notice
severity: down
# affected sections (array). Uncomment the affected one's
affected:
# - matrix
# - xmpp
# - diaspora
# - gitlab
# - website
# - mailing-lists
# - loomio
  - videos
# - planet
# - jitsi

# Don't change the value below
section: issue
---
Service was down due to lack of sufficient storage space.
